const geoip = require('geoip-lite');
const functions = require('firebase-functions');

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

exports.geoip = functions.https.onRequest((req, res) => {
    const ip = req.query.ip;
    if(ip && ip.length >= 7){
        var geo = geoip.lookup(ip);
        if(geo){
            var result = {
                range: geo.range || [],
                country: geo.country || '',
                region: geo.region || '',
                city: geo.city || '',
                ll: geo.ll || [],
                metro: geo.metro || '',
                zip: geo.zip || ''
            };
            res.send(result);
        }
    }
    res.send({});
});
